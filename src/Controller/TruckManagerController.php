<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Form\VehicleTruckType;
use App\Form\VehicleCarType;
use App\Form\ConstructorType;

use App\Repository\VehicleRepository;
use App\Repository\ConstructorRepository;

use App\Entity\Vehicle;
use App\Entity\Truck;
use App\Entity\Car;
use App\Entity\Constructor;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class TruckManagerController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('truck_manager/index.html.twig', [
            'controller_name' => 'TruckManagerController',
        ]);
    }



    /**
     * @Route("/newTruck", name="create_truck")
     */
    public function newTruck(ObjectManager $manager, Request $request, ConstructorRepository $repo)
    {
        if( count( $constructorExist = $repo->findAll()) <= 0){
            return $this->render("truck_manager/alertMessage.html.twig", [
                "alertMessage" => "Votre liste de constructeur est vide", 
                "alertDescription" => "Il est nécessaire d'avoir au moins un constructeur de véhicules pour pouvoir créer un véhicule."
                ]);
        }

        $vehicle = new Vehicle();
        $form = $this->createForm(VehicleTruckType::class, $vehicle); 
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $vehicle->setCreatedAt(new \DateTime);
            $vehicle->setType('truck');
            $manager->persist($vehicle);
            $manager->flush();
            return $this->redirectToRoute('consult_vehicle');
        }
        
        return $this->render('truck_manager/truckForm.html.twig', [
            'controller_name' => 'TruckManagerController',
            'button' => 'Ajouter ce camion',
            'title' => 'Crééer un véhicule: camion',
            'formVehicle' => $form->createView(),
        ]);
    }


    /**
     * @Route("/newCar", name="create_car")
     */
    public function newCar(ObjectManager $manager, Request $request, ConstructorRepository $repo)
    {

        if( count( $constructorExist = $repo->findAll()) <= 0){
            return $this->render("truck_manager/alertMessage.html.twig", [
                "alertMessage" => "Votre liste de constructeur est vide", 
                "alertDescription" => "Il est nécessaire d'avoir au moins un constructeur de véhicules pour pouvoir créer un véhicule."
                ]);
        }

        $vehicle = new Vehicle();
        $form = $this->createForm(VehicleCarType::class, $vehicle); 
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $vehicle->setCreatedAt(new \DateTime);
            $vehicle->setType('car');
            $manager->persist($vehicle);
            $manager->flush();
            return $this->redirectToRoute('consult_vehicle');
        }
        
        return $this->render('truck_manager/carForm.html.twig', [
            'controller_name' => 'TruckManagerController',
            'button' => 'Ajouter ce véhicule',
            'title' => 'Créer un véhicule: voiture',
            'formVehicle' => $form->createView(),
        ]);
    }
    /**
     * @Route("/newConstructor", name="create_constructor")
     */
    public function newConstructor(ObjectManager $manager, Request $request)
    {

        $constructor = new Constructor();
        $form = $this->createForm(ConstructorType::class, $constructor); 
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($constructor);
            $manager->flush();
            return $this->redirectToRoute('consult_constructor');
        }
        
        return $this->render('truck_manager/constructorForm.html.twig', [
            'controller_name' => 'TruckManagerController',
            'button' => 'Ajouter cette marque',
            'title' => 'Nouvelle marque',
            'formConstructor' => $form->createView(),
        ]);
    }
    /**
     * @Route("/editConstructor/{id}", name="edit_constructor")
     */
    public function editConstructor(ObjectManager $manager, Request $request, Constructor $constructor)
    {

        $form = $this->createForm(ConstructorType::class, $constructor); 
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($constructor);
            $manager->flush();
            return $this->redirectToRoute('consult_constructor');
        }
        
        return $this->render('truck_manager/constructorForm.html.twig', [
            'controller_name' => 'TruckManagerController',
            'button' => 'Enregistrer les modifications',
            'title' => 'Modifier une marque',
            'formConstructor' => $form->createView(),
        ]);
    }


    /**
     * @Route("/editCar/{id}", name="edit_car")
     */
    public function editCar(Vehicle $vehicle, ObjectManager $manager, Request $request)
    {


        $form = $this->createForm(VehicleCarType::class, $vehicle); 
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($vehicle);
            $manager->flush();
            return $this->redirectToRoute('consult_vehicle');
        }
        
        return $this->render('truck_manager/carForm.html.twig', [
            'controller_name' => 'TruckManagerController',
            'button' => 'Enregistrer les modifications',
            'title' => 'Modifier un véhicule: voiture',
            'formVehicle' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/editTruck", name="edit_truck")
     */
    public function editTruck(Vehicle $vehicle, ObjectManager $manager, Request $request)
    {

        $form = $this->createForm(VehicleTruckType::class, $vehicle); 
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($vehicle);
            $manager->flush();
            return $this->redirectToRoute('consult_vehicle');
        }
        
        return $this->render('truck_manager/truckForm.html.twig', [
            'controller_name' => 'TruckManagerController',
            'button' => 'Enregistrer les modifications',
            'title' => 'Modifier un véhicule: camion',
            'formVehicle' => $form->createView(),
        ]);
    }

    /**
     * @Route("/consultVehicle", name="consult_vehicle")
     */
    public function consultVehicle(ObjectManager $manager, VehicleRepository $repoVehicle)
    {

        if( count( $repoVehicle->findAll()) <= 0){
            return $this->render("truck_manager/alertMessageNoVehicle.html.twig", [
                "alertMessage" => "Aucun vehicule enregistré", 
                "alertDescription" => "Vous n'avez pas encore créé de véhicule. Vous pouvez en ajouter un via le menu."
                ]);
        }

        $vehicleList = $repoVehicle->findAll();

        
        
        return $this->render('truck_manager/consultVehicle.html.twig', [
            'controller_name' => 'TruckManagerController',
            'vehicles' => $vehicleList,
        ]);
    }
    /**
     * @Route("/consultTruck", name="consult_truck")
     */
    public function consultTruck(ObjectManager $manager, VehicleRepository $repoVehicle)
    {

        $vehicleList = $repoVehicle->findBy(['type' => 'truck']);

        
        
        return $this->render('truck_manager/consultVehicle.html.twig', [
            'controller_name' => 'TruckManagerController',
            'vehicles' => $vehicleList,
        ]);
    }
    /**
     * @Route("/consultCar", name="consult_car")
     */
    public function consultCar(ObjectManager $manager, VehicleRepository $repoVehicle)
    {

        $vehicleList = $repoVehicle->findBy(['type' => 'car']);

        
        
        return $this->render('truck_manager/consultVehicle.html.twig', [
            'controller_name' => 'TruckManagerController',
            'vehicles' => $vehicleList,
        ]);
    }
    
    /**
     * @Route("/consultConstructor", name="consult_constructor")
     */
    public function consultConstructor(ObjectManager $manager, ConstructorRepository $repoConstructor)
    {

        $constructorList = $repoConstructor->findAll();

        
        
        return $this->render('truck_manager/consultConstructor.html.twig', [
            'controller_name' => 'TruckManagerController',
            'constructors' => $constructorList,
        ]);
    }

    
    /**
     * @Route("/deleteVehicle/{id}", name="delete_vehicle")
     */
    public function deleteVehicle(ObjectManager $manager, Vehicle $vehicle)
    {

        $manager->remove($vehicle);
        $manager->flush();

        return $this->redirectToRoute('consult_vehicle');
    }
    /**
     * @Route("/deleteConstructor/{id}", name="delete_constructor")
     */
    public function deleteConstructor(ObjectManager $manager, Constructor $constructor)
    {

        $manager->remove($constructor);
        $manager->flush();

        return $this->redirectToRoute('consult_constructor');
    }
   
}
