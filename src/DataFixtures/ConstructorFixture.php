<?php

namespace App\DataFixtures;

use App\Entity\Constructor;
use App\Entity\Truck;
use App\Entity\Car;
use App\Entity\Vehicle;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ConstructorFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = \Faker\Factory::create("fr_FR");
        $brands = [
            "Ford",
            "Nissan",
            "Renault",
            "Peugeot",
            "Citroën"
        ];
            
        for ($j=0; $j < count($brands) ; $j++) {
                $constructor = new Constructor();
                $constructor = $constructor->setName($brands[$j])
                                           ->setPictureURL("http://placehold.it/150x150");
                $manager->persist($constructor);
                
                
                for ($i=0; $i < mt_rand(5,10) ; $i++) { 
                    $type = mt_rand(1,2);
                    $vehicle = new Vehicle();
                    $vehicle = $vehicle ->setName($faker->word())
                    ->setColor($faker->colorName())
                    ->setConstructor($constructor)
                    ->setCreatedAt($faker->dateTimeBetween("- 2 years"))
                    ;
                    
                    if($type == 1){
                        $vehicle->setType('truck');
                        $truck = new Truck();
                        $truck = $truck ->setWheelNumber(mt_rand(2,6)*2)
                        ->setWeight(mt_rand(300000,700000)/100)
                        ->setVehicleId($vehicle);
                        $manager->persist($truck);
                    }else{
                        $car = new Car();
                        $vehicle->setType('car');
                        $isConvertible = mt_rand(0,1);
                        if($isConvertible == 0){
                            $car = $car->setConvertible(true);
                        }else{
                            $car = $car->setConvertible(false);
                        }
                        $car->setVehicleId($vehicle);
                        $manager->persist($car);
                    }
                    $manager->persist($vehicle);
                }
        } 
        $manager->flush();
    }
}
