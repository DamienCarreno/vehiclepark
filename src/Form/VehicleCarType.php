<?php

namespace App\Form;

use App\Entity\Vehicle;
use App\Entity\Car;
use App\Entity\Constructor;

use App\Form\CarType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class VehicleCarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('color')
            ->add('constructor', EntityType::class, ["class" => Constructor::class, "choice_label" => "name"])
            ->add('Car', CarType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vehicle::class,
        ]);
    }
}
