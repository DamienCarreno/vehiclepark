<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TruckRepository")
 */
class Truck
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $weight;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThan( value = 3 )
     *     
     */
    private $wheelNumber;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vehicle", inversedBy="truck", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $vehicle_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getWheelNumber(): ?int
    {
        return $this->wheelNumber;
    }

    public function setWheelNumber(int $wheelNumber): self
    {
        $this->wheelNumber = $wheelNumber;

        return $this;
    }

    public function getVehicleId(): ?Vehicle
    {
        return $this->vehicle_id;
    }

    public function setVehicleId(Vehicle $vehicle_id): self
    {
        $this->vehicle_id = $vehicle_id;

        return $this;
    }
}
