<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarRepository")
 */
class Car
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $convertible;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vehicle", inversedBy="car", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $vehicle_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConvertible(): ?bool
    {
        return $this->convertible;
    }

    public function setConvertible(bool $convertible): self
    {
        $this->convertible = $convertible;

        return $this;
    }

    public function getVehicleId(): ?Vehicle
    {
        return $this->vehicle_id;
    }

    public function setVehicleId(Vehicle $vehicle_id): self
    {
        $this->vehicle_id = $vehicle_id;

        return $this;
    }
}
